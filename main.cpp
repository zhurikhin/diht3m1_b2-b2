#include <iostream>
#include <vector>
#include <sstream>

class ShiftsFunc
{
protected:
  std::vector<size_t> shifts_table;

public:
  virtual void load(std::string const& str) = 0;
  virtual std::string min_str() const = 0;

  void load(std::vector<size_t> const& table)
  {
    shifts_table = table;
  }

  ShiftsFunc() = default;

  size_t operator()(size_t index) const
  {
    return shifts_table.at(index);
  }

  operator std::string() const
  {
    return min_str();
  }
};

class ZFunc;

class PrefixFunc : public ShiftsFunc
{
  friend class ZFunc;
public:
  using ShiftsFunc::load;
  virtual void load(std::string const& str) final;
  virtual std::string min_str() const final;
  PrefixFunc() = default;

  void load(ZFunc const& z_func);
  template <typename T>
  PrefixFunc(T const& source)
  {load(source);};
};

class ZFunc : public ShiftsFunc
{
  friend class PrefixFunc;
public:
  using ShiftsFunc::load;
  virtual void load(std::string const& str) final;
  virtual std::string min_str() const final;

  ZFunc() = default;

  template <typename T>
  ZFunc(T const& source)
  {load(source);};
};

void PrefixFunc::load(std::string const& str)          // str ---> prefix
{
  size_t str_len = str.length();
  shifts_table = std::vector<size_t>(str_len, 0);
  size_t max_prefix = 0;

  for (size_t s = 1; s < str_len; ++s)
  {
    while (max_prefix > 0 and str[max_prefix] != str[s])
      max_prefix = shifts_table[max_prefix - 1];
    if (str[max_prefix] == str[s])
      ++max_prefix;
    shifts_table[s] = max_prefix;
  }
}

std::string PrefixFunc::min_str() const                // prefix ---> str
{
  size_t table_size = shifts_table.size();
  std::string result = "";
  std::string support_elements(table_size, '\0');
  if (table_size > 0)
  {
    result += 'a';
    support_elements[0] = 'b';
    for (size_t i = 1; i < table_size; ++i)
    {
      size_t previous = i - 1;
      intmax_t predecessor = shifts_table[i] - 1;
      if (shifts_table[i] == 0)
        result += support_elements[previous];
      else
        result += result[predecessor];

      if (predecessor >= 0)
        support_elements[i] = std::max(support_elements[predecessor],
            (char)(result[predecessor + 1] + 1));
      else
        support_elements[i] = 'b';
    }
  }
  return result;
}

void PrefixFunc::load(ZFunc const& z_func)             // z ---> prefix
{
  size_t zfunc_size = z_func.shifts_table.size();
  shifts_table = std::vector<size_t>(zfunc_size, 0);

  for (size_t i = 1; i < zfunc_size; ++i)
  {
    for (intmax_t j = (intmax_t)z_func(i) - 1; j >= 0; --j)
    {
      if (shifts_table[i + j] > 0)
        break;
      else
        shifts_table[i + j] = (size_t)j + 1;
    }
  }
}

void ZFunc::load(PrefixFunc const& prefix_func)         // prefix ---> z
{
  load(prefix_func.min_str());
}

void ZFunc::load(std::string const& str)                // str ---> z
{
  size_t str_len = str.length();
  shifts_table = std::vector<size_t>(str_len, 0);

  for (size_t i = 0, match_lbound = 0, match_rbound = 0; i < str_len; ++i)
  {
    if (i <= match_rbound)
      shifts_table[i] = std::min((match_rbound - i + 1),
          shifts_table[i - match_lbound]);
    while (i + shifts_table[i] < str_len
           and str[shifts_table[i]] == str[i + shifts_table[i]])
      ++shifts_table[i];
    if (i + shifts_table[i] - 1 > match_rbound)
      match_lbound = i, match_rbound = i + shifts_table[i] - 1;
  }
}

std::string ZFunc::min_str() const                      // z ---> str
{
  std::string result = "";
  if(shifts_table.size() == 0)
    return result;

  result += 'a';

  std::vector<size_t> used_indices;
  bool prefix_written = true;

  for (size_t i = 1; i < shifts_table.size(); ) {
    if (shifts_table[i] != 0) {
      used_indices.clear();
      size_t prefix_index = 0;
      size_t remaining_block_len = shifts_table[i];

      while (remaining_block_len > 0) {
        if (shifts_table[i] > remaining_block_len) {
          remaining_block_len = shifts_table[i];
          used_indices.push_back(shifts_table[i]);
          prefix_index = 0;
        }

        if (shifts_table[i] == remaining_block_len) {
          used_indices.push_back(shifts_table[i]);
        }

        result += result[prefix_index++];
        ++i;
        --remaining_block_len;
      }

      prefix_written = true;
    } else {
      if (prefix_written) {
        std::vector<int> used_chars(26, false);
        for (size_t n : used_indices) {
          used_chars[result[n] - 'a'] = true;
        }

        char c = 'b';
        while (used_chars[c - 'a']) {
          c++;
        }
        result += c;

        prefix_written = false;
      } else {
        result += 'b';
      }
      ++i;
    }
  }

  return result;

  //return PrefixFunc(*this).min_str();
}

#define CONTEST_B2

int main()
{
  std::vector<size_t> shifts_table;

  size_t i = 0;
  while(std::cin >> i)
  {
    shifts_table.push_back(i);
  }

  if(shifts_table.size())
    shifts_table[0] = 0;

  #ifdef CONTEST_B1
  std::cout << PrefixFunc(shifts_table).min_str();
  #endif

  #ifdef CONTEST_B2
  std::cout << ZFunc(shifts_table).min_str();
  #endif

  return 0;
}